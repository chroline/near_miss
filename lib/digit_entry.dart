import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DigitEntryRow extends StatelessWidget {
  final int compare;
  final List<int?> row;
  final void Function(int column, int value) onDigitUpdate;

  const DigitEntryRow(
      {Key? key,
      required this.compare,
      required this.row,
      required this.onDigitUpdate})
      : super(key: key);

  @override
  Widget build(BuildContext context) => IntrinsicHeight(
        child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: prefixPadding, right: 15),
                child: Text(compare.toString(),
                    style: TextStyle(
                      fontSize: 20,
                      fontFeatures: [FontFeature.tabularFigures()],
                    )),
              )
            ]..addAll(row
                .asMap()
                .entries
                .map((entry) => SizedBox(
                      width: 40,
                      child: _DigitEntry(
                        text: (row[entry.key] == null
                            ? ""
                            : row[entry.key].toString()),
                        onChanged: (value) =>
                            onDigitUpdate(entry.key, int.parse(value)),
                      ),
                    ))
                .toList())),
      );

  double get prefixPadding =>
      (TextPainter(
              text: TextSpan(
                  text: "0",
                  style: TextStyle(
                    fontSize: 20,
                    fontFeatures: [FontFeature.tabularFigures()],
                  )),
              maxLines: 1,
              textDirection: TextDirection.ltr)
            ..layout(minWidth: 0, maxWidth: double.infinity))
          .size
          .width *
      (3 - compare.toString().length);
}

class _DigitEntry extends StatefulWidget {
  final String text;
  final void Function(String) onChanged;

  const _DigitEntry({Key? key, required this.text, required this.onChanged})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _DigitEntryState();
}

class _DigitEntryState extends State<_DigitEntry> {
  final controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    controller.text = widget.text;

    return TextFormField(
      textAlign: TextAlign.center,
      keyboardType: TextInputType.number,
      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
      controller: controller,
      readOnly: widget.text != "",
      maxLength: 1,
      decoration: InputDecoration(
        counterText: "",
      ),
      onChanged: widget.onChanged,
    );
  }
}
