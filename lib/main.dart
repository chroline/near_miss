import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:in_app_review/in_app_review.dart';
import 'package:near_miss/new_number_snackbar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import 'digit_entry.dart';

late SharedPreferences prefs;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  prefs = await SharedPreferences.getInstance();

  runApp(NearMissApp());
}

class NearMissApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Near Miss",
      theme: ThemeData(primarySwatch: Colors.red),
      home: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: _NearMissGame()),
    );
  }
}

class _NearMissGame extends StatefulWidget {
  @override
  State createState() => _NearMissGameState();
}

class _NearMissGameState extends State {
  final List<List<int?>> digits = [
    [null],
    [null],
    [null, null],
    [null, null],
    [null, null],
    [null, null, null]
  ];

  List<List<int>> history = [];

  final fabKeys = [Key("number"), Key("score")];

  onDigitUpdate(int row) => (int column, int value) => setState(() {
        digits[row][column] = value;
        history.add([row, column]);
      });

  undo() => setState(() {
        try {
          final lastEdit = history.removeLast();
          print(lastEdit);
          digits[lastEdit[0]][lastEdit[1]] = null;
        } catch (e) {}
      });

  clear() {
    FocusScope.of(context).unfocus();
    setState(() {
      digits.asMap().entries.forEach((row) => row.value
          .asMap()
          .entries
          .forEach((column) => digits[row.key][column.key] = null));
      history = [];
    });
  }

  generateNewNumber() => ScaffoldMessenger.of(context)
      .showSnackBar(createNewNumberSnackbar(Random().nextInt(10)));

  bool get isComplete => digits.expand((i) => i).toList().indexOf(null) == -1;

  showScore() {
    FocusScope.of(context).unfocus();

    int rowToInt(int row) =>
        int.parse(digits[row].fold("", (a, b) => a + b.toString()));

    int total = (5 - rowToInt(0)).abs() +
        (10 - rowToInt(1)).abs() +
        (20 - rowToInt(2)).abs() +
        (50 - rowToInt(3)).abs() +
        (100 - rowToInt(4)).abs() +
        (100 - rowToInt(5)).abs();

    requestReview();

    showDialog(
        context: context,
        builder: (context) => AlertDialog(
                title: Text("Your score: $total"),
                content: Text('Would you like to clear your board?'),
                actions: [
                  TextButton(
                    child: Text('CLEAR'),
                    onPressed: () {
                      clear();
                      Navigator.of(context).pop();
                    },
                  )
                ]));
  }

  requestReview() {
    final now = DateTime.now();
    final lastWeek = DateTime(now.year, now.month, now.day - 7);

    if (!prefs.containsKey("lastReviewDate")) {
      prefs.setString("lastReviewDate",
          DateTime.now().subtract(Duration(days: 7)).toString());
    }
    final lastReviewDate =
        DateTime.parse(prefs.getString("lastReviewDate") as String);

    if (lastReviewDate.difference(lastWeek).inDays <= 0) {
      InAppReview.instance.requestReview();
      prefs.setString("lastReviewDate", DateTime.now().toString());
    }
  }

  @override
  Widget build(BuildContext build) {
    return Scaffold(
        appBar: appBar,
        floatingActionButton: floatingActionButton,
        body: Center(
          child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                DigitEntryRow(
                  compare: 5,
                  row: digits[0],
                  onDigitUpdate: onDigitUpdate(0),
                ),
                DigitEntryRow(
                  compare: 10,
                  row: digits[1],
                  onDigitUpdate: onDigitUpdate(1),
                ),
                DigitEntryRow(
                  compare: 20,
                  row: digits[2],
                  onDigitUpdate: onDigitUpdate(2),
                ),
                DigitEntryRow(
                  compare: 50,
                  row: digits[3],
                  onDigitUpdate: onDigitUpdate(3),
                ),
                DigitEntryRow(
                  compare: 100,
                  row: digits[4],
                  onDigitUpdate: onDigitUpdate(4),
                ),
                DigitEntryRow(
                  compare: 100,
                  row: digits[5],
                  onDigitUpdate: onDigitUpdate(5),
                ),
                SizedBox(height: 30),
                footer
              ]),
        ));
  }

  AppBar get appBar => AppBar(
        leading: IconButton(
          icon: Icon(Icons.help_rounded),
          onPressed: () =>
              launch("https://projects.colegaw.in/near-miss"),
        ),
        title: Text("Near Miss"),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.undo_rounded),
            onPressed: undo,
          ),
          IconButton(
            icon: Icon(Icons.delete_rounded),
            onPressed: clear,
          )
        ],
      );

  FloatingActionButton get floatingActionButton => !isComplete
      ? FloatingActionButton.extended(
          key: fabKeys[0],
          onPressed: generateNewNumber,
          icon: Icon(Icons.add_rounded),
          label: Text("NUMBER"),
        )
      : FloatingActionButton.extended(
          key: fabKeys[1],
          onPressed: showScore,
          icon: Icon(Icons.calculate_rounded),
          label: Text("SCORE"),
        );

  Widget get footer => SizedBox(
      width: 180,
      child: Text(
        "Built by Cole Gawin, 2021",
        textAlign: TextAlign.center,
        style: TextStyle(color: Colors.grey.shade500, fontSize: 12),
      ));
}
