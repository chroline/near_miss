import 'dart:math';

import 'package:flutter/material.dart';

SnackBar createNewNumberSnackbar(int number) => SnackBar(
      behavior: SnackBarBehavior.floating,
      content: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          style: TextStyle(color: Colors.white),
          children: <TextSpan>[
            TextSpan(text: 'Next number: '),
            TextSpan(
                text: number.toString(),
                style: TextStyle(fontWeight: FontWeight.bold)),
          ],
        ),
      ),
    );
